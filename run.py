
from qwen_cpp import Pipeline
from sys import stdout
import sys

question=sys.argv[1]
chat=Pipeline(model_path='../models/qwen-7b-ggml/qwen7b-ggml.bin',tiktoken_path='../models/qwen-7b-ggml/qwen.tiktoken')
ss=chat.chat([question], temperature=0.3, num_threads=8, stream=True)
for i in ss:
  stdout.write(i)
  stdout.flush()
stdout.write("\n")

